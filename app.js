
const fs = require('fs');
let json = require('./config-filters');
var goodFilters = [];

// lecture des fichiers du répertoire filters
fs.readdir('./filters', (err,files) => {

    files.forEach(file => {

        if (file.split('.')[1] === 'js'){
            const test = require('./filters/' + file);
            if (typeof (test) !== "function"){
                throw new Error( test + " dans " + file + " N'EST PAS UNE FONCTION !");
            }else{
                goodFilters.push(file);
            }
        }else{
            throw new Error("FORMAT DE FICHIER INVALIDE : le dossier /filters ne doit contenir que des fichiers .js !");
        }

    });

    if (goodFilters.length !== 0){
        console.log("Liste des filtres valides : " + goodFilters);
    }else{
        console.warn("Aucun filtres valide !");
    }

    if (Array.isArray(json.steps)){

        const listOfId = [];
        for (let config of json.steps){

            if (config.id !== undefined){

                if (config.filter !== undefined ){
                    if (goodFilters.includes(config.filter)){

                        if (config.next !== undefined){

                            listOfId.push(config.id);
                        }else{
                            throw new Error("CHAQUE STEP DOIT AVOIR UN NEXT (next : '2' (de type string)) !");
                        }

                    }else {
                        throw new Error("Le fichier " + config.filter + " est invalide ou n'existe pas !");
                    }

                }else{
                    throw new Error("CHAQUE STEP DOIT AVOIR UN FILTER (filter : fichier.js) !");
                }

            }else{
                throw new Error("CHAQUE STEP DOIT AVOIR UN IDENTIFIANT (id : '1' (de type string)) !");
            }
        }

        let min = Math.min(...listOfId);

        executeStep(min, null);

    }else{
        throw new Error("steps dans config-filters DOIT ETRE UN TABLEAU !");
    }
});

function executeStep(stepToExecute, data) {

    for (let config of json.steps){

        if (config.id === String(stepToExecute)){
            const fonction = require('./filters/' + config.filter);
            if (config.params !== undefined){

                resultat = fonction(config.params, data)

            }else{
                resultat = fonction([],data);
            }
            if (config.next === ""){
                return resultat;
            }else{
                executeStep(config.next, resultat)
            }

        }
    }

}





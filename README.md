# Notre Framework
### BILLET Simon, GARAULT Alexandre et BITAUD Clémence
## Introduction

Ce nouveau framework permet d'implémenter le pattern PIPE/FILTER très simplement. Grâce au fichier `config-filters.json` vous pourrez lister tous les filtres que vous aurez créés. Il sera aussi possible de définir les dépendances qu'il y a entre eux. Vous pourrez définir tous vos filtres dans le dossier `filters`.

## Getting started

Le framework se prend très rapidement en main. Grâce à l'outil `ppft`, il vous permettra de créer et supprimer rapidement les éléments essentiel à vos travaux comme le filter et les steps. Vous retrouverez son focntionnement dans la section Tools de ce document un peu plus bas.

Les fonction de base de notre framework est d'implémenter le pattern PIPE/FILTER en stockant tous ces filtres (un fichier par filtre) au sein d'un même dossier. Ces filtres sont tous listé dans le fichier `config-filters.json` qui permet de connaitre les dépendances entre chaque filtres et les données en entrés.

## API

### Architecture du `config-filters.json`

Ci-dessous vous pouvez retrouver le façon dont écrit le fichier `config-filters.json` se truvant à la racine du projet.
```json
"steps" : {
    "1": {
        "filter": "<filter_1>",
        "params" : ['<extra_param_1>' , '<extra_param_2>' , ...],
        "next": "2"
    }, 
    "2": {
        "filter": "<filter_2>",
        "params" : ['<extra_param_1>' , '<extra_param_2>' , ...],
        "next": "3"
    },
    ...
}
```

L'objet JSON `step` pourra contenir n éléments enfants en sont sein qui correspondront à l'id unique de chacun de nos filtres. Chaque filtre contiendra un nom qui sera contenu dans `filter` sous la forme **nomFichier.js**. Les paramètres seront stocker dans une liste nommé `params`. Et si le filtre doit passer par un autre filtre par la suite alors il on stockera l'id de celui dans `next` en format string.

Vos fichiers de filtres devront toujours prendre en paramètre au minimum 2 éléments qui sont `param` et `data`, bien sur les données en leurs sein peuvent être nul. De plus les filtres retourne toujours quelques chose même si l'élèment retourner est nul.

## Errors

 - `nomTest dans nomFile N'EST PAS UNE FONCTION !` : les fichiers js ne doivent contenir que des fonctions
 - `FORMAT DE FICHIER INVALIDE : le dossier /filters ne doit contenir que des fichiers .js !` : Un fichier au sein du dossier `filters` n'est pas un fichier `.js`
 - `Le fichier config-filters.json est invalide ou n'existe pas !` : Le fichier permettant de lister les filtres et leurs dépendances est non trouvable.
 - `CHAQUE STEP DOIT AVOIR UN FILTER (filter : fichier.js) !` : Une step doit posséder un filtre connu et existant dans le dossier /filters
 - `CHAQUE STEP DOIT AVOIR UN IDENTIFIANT (id : 1 (de type string) !` : un identifant (id) doit être définit dans le fichier `config-filters.json`
 - `CHAQUE STEP DOIT AVOIR UN NEXT (next : '2' (de type string)) !` : une étape suivante (next) doit être définit dans le fichier `config-filters.json`

## Tools

### Installation

Si vous n'avez toujours pas installé le module `ppft` sur votre machine voici comment faire, lancer la commande suivante :
```
npm install ppft -g
```

### Outil `ppft`

`ppft new` : Initie un projet avec un fichier vierge ou un filter de type hello world. Exemple : 
 - `ppft new nom_projet` : Initiera un projet avec le comme nom : `nom_projet`

`ppft add_filter` : Ajoute un nouveau filter dans le projet. Exemple : 
 - `ppft add_filter filter_lower` : Créera un filter ayant pour nom : `filter_lower`

`ppft del_filter` : Supprime un filter dans votre projet. Attention la commande supprimera le filter uniquement si celle-ci n'est pas utilisé. Exemple : 
 - `ppft del_filter filter_lower` : Supprimera le filter ayant pour nom : `filter_lower`

`ppft add_step` : Ajoute une step de configuration au fichier `config-filters.json` du projet. Exemple : 
 - `ppft add_step 12 step_lower 28` : Créera une step ayant pour nom : `step_lower` ayant pour id unique `12` ayant pour step `28`


`ppft del_step` : Supprimer une step de configuration du fichier `config-filters.json`. Exemple : 
 - `ppft add_step 12` : Supprimera la step ayant pour id `12` ayant pour step `28`
